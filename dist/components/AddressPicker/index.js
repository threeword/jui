(wx["webpackJsonp"] = wx["webpackJsonp"] || []).push([["components/AddressPicker/index"],{

/***/ "./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/components/AddressPicker/index.tsx?taro&type=script&parse=COMPONENT&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./src/components/AddressPicker/index.tsx?taro&type=script&parse=COMPONENT& ***!
  \*******************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _taroWeapp = __webpack_require__(/*! @tarojs/taro-weapp */ "./node_modules/@tarojs/taro-weapp/index.js");

var _taroWeapp2 = _interopRequireDefault(_taroWeapp);

__webpack_require__(/*! ./style.scss */ "./src/components/AddressPicker/style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SELECT_TIP = ["选择省", "选择市", "选择区县", "选择街道"];
var Page = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Page, _BaseComponent);

  function Page() {
    var _ref,
        _this2 = this;

    var _temp, _this, _ret;

    _classCallCheck(this, Page);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Page.__proto__ || Object.getPrototypeOf(Page)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["loopArray14", "showCommit", "selectObj", "selectIndex", "showNext", "value", "options", "title", "SELECT_TIP", "showSelect", "getAreaApi", "__fn_onClick", "defaultValue"], _this.selectOption = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var _this$props$defaultVa, defaultValue, _initValue, lastSelectOption, lastSelectIndex;

      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this$props$defaultVa = _this.props.defaultValue, defaultValue = _this$props$defaultVa === undefined ? [] : _this$props$defaultVa;

              if (!(defaultValue.length > 0 && _this.state.selectObj.length == 0)) {
                _context.next = 11;
                break;
              }

              _context.next = 4;
              return _this.initData(defaultValue, [], []);

            case 4:
              _initValue = _context.sent;
              lastSelectOption = _initValue.result[_initValue.result.length - 1];
              lastSelectIndex = _initValue.options.findIndex(function (option) {
                return option.code == lastSelectOption.code;
              });

              _this.options = _initValue.options;
              _this.setState({
                selectObj: _initValue.result,
                value: [lastSelectIndex],
                selectIndex: _initValue.result.length - 1,
                showNext: !!lastSelectOption.hasChildren
              });
              _context.next = 12;
              break;

            case 11:
              _this.selectNext();

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, _this2);
    })), _this.initData = function () {
      var _ref3 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee2(value, result, options) {
        var _options;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(value.length == result.length)) {
                  _context2.next = 4;
                  break;
                }

                return _context2.abrupt("return", { result: result, options: options });

              case 4:
                _context2.next = 6;
                return _this.props.getAreaApi(result.length == 0 ? 0 : result[result.length - 1].code);

              case 6:
                _options = _context2.sent;

                result[result.length] = _options.find(function (o) {
                  return o.code == value[result.length];
                });
                _context2.next = 10;
                return _this.initData(value, result, _options);

              case 10:
                return _context2.abrupt("return", _context2.sent);

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, _this2);
      }));

      return function (_x, _x2, _x3) {
        return _ref3.apply(this, arguments);
      };
    }(), _this.onChange = function (e) {
      if (_this.nextLoading) {
        return;
      }var _this$state = _this.state,
          selectObj = _this$state.selectObj,
          selectIndex = _this$state.selectIndex;

      var value = e.detail.value;
      /* 改变时赋值，并判断是否有下一级 */
      selectObj[selectIndex] = _extends({}, _this.options[value[0]]);
      _this.setState({
        selectObj: selectObj,
        value: value,
        showNext: selectObj[selectIndex].hasChildren
      });
      _this.props.onChange(selectObj.map(function (o) {
        return o.name;
      }), selectObj.map(function (o) {
        return o.code;
      }));
    }, _this.selectNext = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
      var _this$state2, selectObj, selectIndex, nowSelectObj;

      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              if (!_this.nextLoading) {
                _context3.next = 2;
                break;
              }

              return _context3.abrupt("return");

            case 2:
              _this.nextLoading = true;
              _this$state2 = _this.state, selectObj = _this$state2.selectObj, selectIndex = _this$state2.selectIndex;
              nowSelectObj = selectObj[selectIndex];

              _this.options = [];
              /* 当前下一级 */
              _this.setState({
                selectIndex: selectIndex + 1
              });
              /* 请求数据 */
              _context3.next = 9;
              return _this.getArea(nowSelectObj && nowSelectObj.code);

            case 9:
              /* 默认选中下一级第一个 */
              if (_this.options.length > 0) {
                selectObj[selectIndex + 1] = _extends({}, _this.options[0]);
                _this.setState({
                  value: [0],
                  selectObj: selectObj,
                  showNext: !!selectObj[selectIndex + 1].hasChildren
                });
                _this.props.onChange(selectObj.map(function (o) {
                  return o.name;
                }), selectObj.map(function (o) {
                  return o.code;
                }));
              } else {
                _this.setState({ value: [] });
                _this.props.onChange([], []);
              }
              _this.nextLoading = false;

            case 11:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, _this2);
    })), _this.reSelect = function (selectIndex) {
      return _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
        var selectObj, reSelectObj, value, newSelectObj;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                selectObj = _this.state.selectObj;
                reSelectObj = selectObj[selectIndex];
                /* 判断是否是第一集 */

                if (!(selectIndex <= 0)) {
                  _context4.next = 7;
                  break;
                }

                _context4.next = 5;
                return _this.getArea();

              case 5:
                _context4.next = 9;
                break;

              case 7:
                _context4.next = 9;
                return _this.getArea(selectObj[selectIndex - 1].code);

              case 9:
                /* 选中值 */
                value = _this.options.findIndex(function (o) {
                  return o.code == reSelectObj.code;
                });
                newSelectObj = [].concat(_toConsumableArray(selectObj)).slice(0, selectIndex + 1);

                _this.setState({
                  value: [value],
                  selectIndex: selectIndex,
                  selectObj: newSelectObj,
                  showNext: reSelectObj.hasChildren
                });
                _this.props.onChange(newSelectObj.map(function (o) {
                  return o.name;
                }), newSelectObj.map(function (o) {
                  return o.code;
                }));

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, _this2);
      }));
    }, _this.onCommit = function () {
      var selectObj = _this.state.selectObj;

      _this.props.onCommit(selectObj.map(function (o) {
        return o.name;
      }), selectObj.map(function (o) {
        return o.code;
      }));
      _this.props.onClose();
    }, _this.anonymousFunc0Map = {}, _this.customComponents = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Page, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Page.prototype.__proto__ || Object.getPrototypeOf(Page.prototype), "_constructor", this).call(this, props);
      this.state = {
        showSelect: false
      };

      this.options = [];
      this.nextLoading = false;
      this.state = {
        value: [],
        selectObj: [],
        selectIndex: -1,
        showNext: false
      };
      this.$$refs = new _taroWeapp2.default.RefsArray();
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.selectOption();
    }
  }, {
    key: "componentWillReceiveProps",
    value: function () {
      var _ref6 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee5(nextProps) {
        var _nextProps$value, value, _initValue, lastSelectOption, lastSelectIndex;

        return _regenerator2.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _nextProps$value = nextProps.value, value = _nextProps$value === undefined ? [] : _nextProps$value;

                if (value.length > 0 && this.state.selectObj.length == 0) {
                  _initValue = this.initData(value, [], []);

                  console.log(_initValue);
                  lastSelectOption = _initValue.result[_initValue.result.length - 1];
                  lastSelectIndex = _initValue.options.findIndex(function (option) {
                    return option.code == lastSelectOption.code;
                  });

                  this.options = _initValue.options;
                  this.setState({
                    selectObj: _initValue.result,
                    value: [lastSelectIndex],
                    selectIndex: _initValue.result.length - 1,
                    showNext: lastSelectOption.hasChildren
                  });
                }

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function componentWillReceiveProps(_x4) {
        return _ref6.apply(this, arguments);
      }

      return componentWillReceiveProps;
    }()
  }, {
    key: "getArea",
    value: function () {
      var _ref7 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee6(parentId) {
        return _regenerator2.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return this.props.getAreaApi(parentId);

              case 2:
                this.options = _context6.sent;

                this.setState();

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function getArea(_x5) {
        return _ref7.apply(this, arguments);
      }

      return getArea;
    }()
  }, {
    key: "_createData",
    value: function _createData() {
      var _this3 = this;

      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      var options = this.options;
      var _state = this.__state,
          value = _state.value,
          selectIndex = _state.selectIndex,
          selectObj = _state.selectObj,
          showNext = _state.showNext,
          showSelect = _state.showSelect;
      var _props = this.__props,
          showCommit = _props.showCommit,
          onClose = _props.onClose,
          _props$title = _props.title,
          title = _props$title === undefined ? "所在地区" : _props$title;

      var loopArray14 = selectObj.map(function (obj, i) {
        obj = {
          $original: (0, _taroWeapp.internal_get_original)(obj)
        };

        var _$indexKey = "bdzzz" + i;

        _this3.anonymousFunc0Map[_$indexKey] = _this3.reSelect(i);
        return {
          _$indexKey: _$indexKey,
          $original: obj.$original
        };
      });
      Object.assign(this.__state, {
        loopArray14: loopArray14,
        showCommit: showCommit,
        options: options,
        title: title,
        SELECT_TIP: SELECT_TIP
      });
      return this.__state;
    }
  }, {
    key: "funPrivatebczzz",
    value: function funPrivatebczzz() {
      return this.props.onClose.apply(undefined, Array.prototype.slice.call(arguments, 1));
    }
  }, {
    key: "anonymousFunc0",
    value: function anonymousFunc0(_$indexKey) {
      var _anonymousFunc0Map;

      ;

      for (var _len2 = arguments.length, e = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        e[_key2 - 1] = arguments[_key2];
      }

      return this.anonymousFunc0Map[_$indexKey] && (_anonymousFunc0Map = this.anonymousFunc0Map)[_$indexKey].apply(_anonymousFunc0Map, e);
    }
  }]);

  return Page;
}(_taroWeapp.Component), _class.$$events = ["funPrivatebczzz", "onCommit", "anonymousFunc0", "selectNext", "onChange"], _class.$$componentPath = "components/AddressPicker/index", _temp2);
exports.default = Page;

Component(__webpack_require__(/*! @tarojs/taro-weapp */ "./node_modules/@tarojs/taro-weapp/index.js").default.createComponent(Page));

/***/ }),

/***/ "./node_modules/file-loader/dist/cjs.js?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!./node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/components/AddressPicker/index.tsx?taro&type=template&parse=COMPONENT&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/file-loader/dist/cjs.js?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!./node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./src/components/AddressPicker/index.tsx?taro&type=template&parse=COMPONENT& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "components/AddressPicker/index.wxml";

/***/ }),

/***/ "./src/components/AddressPicker/index.tsx":
/*!************************************************!*\
  !*** ./src/components/AddressPicker/index.tsx ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_tsx_taro_type_template_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.tsx?taro&type=template&parse=COMPONENT& */ "./src/components/AddressPicker/index.tsx?taro&type=template&parse=COMPONENT&");
/* harmony import */ var _index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.tsx?taro&type=script&parse=COMPONENT& */ "./src/components/AddressPicker/index.tsx?taro&type=script&parse=COMPONENT&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));




/***/ }),

/***/ "./src/components/AddressPicker/index.tsx?taro&type=script&parse=COMPONENT&":
/*!**********************************************************************************!*\
  !*** ./src/components/AddressPicker/index.tsx?taro&type=script&parse=COMPONENT& ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./index.tsx?taro&type=script&parse=COMPONENT& */ "./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/components/AddressPicker/index.tsx?taro&type=script&parse=COMPONENT&");
/* harmony import */ var _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/components/AddressPicker/index.tsx?taro&type=template&parse=COMPONENT&":
/*!************************************************************************************!*\
  !*** ./src/components/AddressPicker/index.tsx?taro&type=template&parse=COMPONENT& ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!file-loader?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!../../../node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!../../../node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./index.tsx?taro&type=template&parse=COMPONENT& */ "./node_modules/file-loader/dist/cjs.js?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!./node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/components/AddressPicker/index.tsx?taro&type=template&parse=COMPONENT&");
/* harmony import */ var _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_COMPONENT___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./src/components/AddressPicker/style.scss":
/*!*************************************************!*\
  !*** ./src/components/AddressPicker/style.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./src/components/AddressPicker/index.tsx","runtime","taro","vendors"]]]);