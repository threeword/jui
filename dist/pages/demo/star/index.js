(wx["webpackJsonp"] = wx["webpackJsonp"] || []).push([["pages/demo/star/index"],{

/***/ "./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/pages/demo/star/index.tsx?taro&type=script&parse=PAGE&":
/*!*****************************************************************************************************************************************************!*\
  !*** ./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./src/pages/demo/star/index.tsx?taro&type=script&parse=PAGE& ***!
  \*****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _taroWeapp = __webpack_require__(/*! @tarojs/taro-weapp */ "./node_modules/@tarojs/taro-weapp/index.js");

var _taroWeapp2 = _interopRequireDefault(_taroWeapp);

__webpack_require__(/*! ./style.scss */ "./src/pages/demo/star/style.scss");

var _star = __webpack_require__(/*! ./images/star0.png */ "./src/pages/demo/star/images/star0.png");

var _star2 = _interopRequireDefault(_star);

var _star3 = __webpack_require__(/*! ./images/star1.png */ "./src/pages/demo/star/images/star1.png");

var _star4 = _interopRequireDefault(_star3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Page = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Page, _BaseComponent);

  function Page() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Page);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Page.__proto__ || Object.getPrototypeOf(Page)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "$compid__11", "$compid__12", "$compid__13", "starCount1", "starCount2", "starCount3"], _this.config = {
      navigationBarTitleText: '评分'
    }, _this.customComponents = ["Star"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Page, [{
    key: '_constructor',
    value: function _constructor() {
      _get(Page.prototype.__proto__ || Object.getPrototypeOf(Page.prototype), '_constructor', this).apply(this, arguments);

      this.state = {
        starCount1: 2,
        starCount2: 0,
        starCount3: 0
      };
      this.$$refs = new _taroWeapp2.default.RefsArray();
    }
  }, {
    key: '_createData',
    value: function _createData() {
      var _this2 = this;

      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _taroWeapp.genCompid)(__prefix + "$compid__11"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__11 = _genCompid2[0],
          $compid__11 = _genCompid2[1];

      var _genCompid3 = (0, _taroWeapp.genCompid)(__prefix + "$compid__12"),
          _genCompid4 = _slicedToArray(_genCompid3, 2),
          $prevCompid__12 = _genCompid4[0],
          $compid__12 = _genCompid4[1];

      var _genCompid5 = (0, _taroWeapp.genCompid)(__prefix + "$compid__13"),
          _genCompid6 = _slicedToArray(_genCompid5, 2),
          $prevCompid__13 = _genCompid6[0],
          $compid__13 = _genCompid6[1];

      var _state = this.__state,
          starCount1 = _state.starCount1,
          starCount2 = _state.starCount2,
          starCount3 = _state.starCount3;


      this.anonymousFunc0 = function (count) {
        _this2.setState({ starCount1: count });
      };

      this.anonymousFunc1 = function (count) {
        _this2.setState({ starCount2: count });
      };

      var anonymousState__temp = [_star2.default, _star4.default];

      this.anonymousFunc2 = function (count) {
        _this2.setState({ starCount3: count });
      };

      _taroWeapp.propsManager.set({
        "count": starCount1,
        "onChange": this.anonymousFunc0
      }, $compid__11, $prevCompid__11);
      _taroWeapp.propsManager.set({
        "large": true,
        "count": starCount2,
        "onChange": this.anonymousFunc1
      }, $compid__12, $prevCompid__12);
      _taroWeapp.propsManager.set({
        "starImage": anonymousState__temp,
        "maxStar": 8,
        "large": true,
        "count": starCount3,
        "onChange": this.anonymousFunc2
      }, $compid__13, $prevCompid__13);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        $compid__11: $compid__11,
        $compid__12: $compid__12,
        $compid__13: $compid__13
      });
      return this.__state;
    }
  }, {
    key: 'anonymousFunc0',
    value: function anonymousFunc0(e) {
      ;
    }
  }, {
    key: 'anonymousFunc1',
    value: function anonymousFunc1(e) {
      ;
    }
  }, {
    key: 'anonymousFunc2',
    value: function anonymousFunc2(e) {
      ;
    }
  }]);

  return Page;
}(_taroWeapp.Component), _class.$$events = [], _class.$$componentPath = "pages/demo/star/index", _temp2);
exports.default = Page;

Component(__webpack_require__(/*! @tarojs/taro-weapp */ "./node_modules/@tarojs/taro-weapp/index.js").default.createComponent(Page, true));

/***/ }),

/***/ "./node_modules/file-loader/dist/cjs.js?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!./node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/pages/demo/star/index.tsx?taro&type=template&parse=PAGE&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/file-loader/dist/cjs.js?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!./node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./src/pages/demo/star/index.tsx?taro&type=template&parse=PAGE& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "pages/demo/star/index.wxml";

/***/ }),

/***/ "./src/pages/demo/star/images/star0.png":
/*!**********************************************!*\
  !*** ./src/pages/demo/star/images/star0.png ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAFq0lEQVR4XuWaX2gcRRzHf7+7nPQld7czG0yp1OKLligUWgvmxf4BfWmLWi1iQQqBVqiKRfFPQRsRQ0tFBatirQYRirZa2oe+iMWCoFStglREEFOqsYXczK4RUUOyX5lwF9L0cre7s7e7qQf3tL/v789n5+ZmfjNMlh+t9QNEZL7LiegGIrow+xsEwXHXdc9YhumYnON6rtVqq5l5iJnXh/BxJgiCva7rHg9hm6pJLABKqfuI6B1m7o6SLYDnpZSDUTSdto0MwBTPzEfiJpY3CJEAKKXKzPwFEfXFBWB0hUJhVbVaPWvjIyltVACDzLwngeAHhRA7EvBj7SISAK31CBEts45KpC9evLi4r69vIgFfVi5CAxgbG1tTLBY/s4p2uXiDEOJkgv5iucoMQF4mw9AAtNa3ENH3sTA3ES04ACMjI4sqlcqPCc0BlFsAALp9399GROsA3FQv+F8iUkR0iYj6kxgFuQTg+/56AEMAVidRZCsfuQPQgVm+HcOdQog32hl1+vn0JJhB8f+USqWl3d3dY50usJ1/9jxvBQDz/15tZ5zUc2Z+1XGcXUn5s/FjAOwC8LKNkyhaAKOFQuFmx3H8KLpO2bLW2rz9NZ0KMNdvEASbXdc9lla8dnEMgKTW9+1imeenhRBrwximZWMAIK1g9Tj7hBBPpxxz3nAGwHdEtCLNhADcK6X8OM2Y88Uyk+AwALPyS/XT1dXVXy6Xv0w1aJNgZgQ8SUT7MkjkAjPf7jjO+Qxiz4Q0I2ATgBMZJfGJ4zgbmTmzxgibzY/neWaXtyQLCMx81HGcLVnENjGnl8Ja6wNEtDOzJDKEMNMQSXk9cAXrrEbCZR0hpdQlZr425kg4bbuizALCZQBqtdoSZj7BzCsjQDA7ukGztdVa7yWipyJom5keEEI8YukjtLxpT1Aptb1QKAy0aowA+JuZh0ul0uDsba1SajMzfxQ6gyaGAF6QUj5n4yOstmVT1Pf9lVNTUxubOPutq6vrWKVS0c0CjY+P3zY5OfkBES0Nm8hcOwD7pZRmjdLRT+iucNQsPM9bBuAtIrojqnaW/WtCiEct9G2lHQNgIgO4xvO894jo/raZzG/wthBiu4W+pbSjABqRlVLDzGyz33hfCPHgfJXUR9sGAO5cGzMfCSHOzadNBYAJrrV+k4geivsmzV8kM++oVqtew0e9l2laa5ta+QVgbqi8K6U8eAWguAnF0Wmth4jomThao2HmTw0EAGby3QPgsSi+AJwtlUpby+XyTw1daiOgEdDzvG0AhqMkPtvWFMHMf1osuv4QQsw0gFMHYIqp1WrrCoXCqbgQbHUGopRy1fSosnUWV+953j0AsuwKvS6EeDgzAAacUmqAmQ/FhWipG3UcZ3mmAOoQHmfmlyyLiSVn5rszB1D/i3yRiHbHqsJCxMzP5gJAfSQcYuYBi3oiSwF8mBsAABb5vn8EQLPNV+TiQgrO5QaASdjzvOuJyEDo+P2EzBZCrd5M/SLmUcsdZMiXP232S65GgNZ6PxE9EaUCS9uTuQGQwSUNw25rbgBorc3hTMtdneXbniufPqnOBQCl1HXM/GvCBbZ0NzU1tbanp+d0LgBorU2fwPQLUvkA2CKlNJNtdpuh2ZUqpZK6hd4SoGmqBEHwipRy5lQ6FyMgYQCfE9FiIuolokkiOs/M3zLz4Wq1esUW/GoEMDoxMXFjb2/vX2F+T1cjACoWi6srlcrX/1sAjRl+wQDwfX9dEASJtcgWHAAAJc/zRomoJ8xba2PzuxAi9GWPXMwBpiCttTlGsz4BinoLPTcAzEFsEATfWI6AHwD0SynHw/rJDQCTsO16YPYKb0ECiAsBgDkoGWgsb8MWn5ul8NyEa7XaXcVicTeAW9sVA+AUgN2u637VzrbZ81z9BOYmWO8R3Fm/aGEuWzS+PxOROd87LIQ4HKfwhuY/ly4s7VxUFxUAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/pages/demo/star/images/star1.png":
/*!**********************************************!*\
  !*** ./src/pages/demo/star/images/star1.png ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAFxklEQVR4XuWabYhUVRjH/88d751d0OhDUeLOnS20nev2YmhCfskXqC8qlSWhEMKCClYkRS9CuRGJYVSQJZolEUi1JvbBL5EoBIXlFoXrzKbk7p3dsCR7Edmdc3fuE2fckXV2du/r3HvX5ut9/s/L75x75pznHkLAXz6rrQFjDQEGgFvBMEFkgtlkgmnbfKh9wDoeMEzD5OTXc0+LujClYBtAy5x8EOF42cb2uUVxyMk26ue+ABR07VEAHwCY4SVhBl4xTNHpRdNoW88ARov/zG9iSYPgCcDp2bjOFto3DLT7BSB1DF5gmFZ3EB9haT0ByOtaJwFbgwYn0J42s7QhqJ8w9J4AFHTtLIDWEAJfKE8XM9tPQYTgK5AL1wDyLdMWk6IcDRRtjFghWn5bf+lwWP78+okNQFIWQw8AtDtIwc9+SdfqphyAs61oKtlaPqQ1AIkFUGjDDGUovc4mXgpGThZMQImBPwGcA7AojFmQSAC9etMywN7GwMIwipzMR+IAhL3KOwEk5k1tRes9J7tGP68sgpEXDwwrQuhzzuF8owt08k89req8lE3y//16J+PwnvPbOdPaHJ4//56oN5PezMRv+nfhWTk4pIjb7+7D356VDRBQIaMeBdHiBviu65IJq4x+cTCqeE5xKMT9vVMsgPlYrmgtcTaMzkIC4OjCVSK9njPFCxHHnDCcBPAjgHlRJsSER4x+8XmUMSeKRb26to+BdVEnkyJ70Zz+kW+jjlsbT86A5+S0jDoRlt3jlHKf0TfcF3XssfGokEmvBPEXMSXxZXm6WBFnY4Tk4QdDlVPerJggdOVMsTqm2Li8Fc6oO4loU1xJAIgNwpWGSKT7gfqkY4FwVUcor6vnCHSTr5nAfCyEHWXkEK4C0NvSPIuVslwQ57uGwHyegE55tC3o2nYAz7vW1jFk5p1G0XoyiA8v2ro9wbyurldAHQ6NkSEw70tZVufYY20+q60ixgEvSYyzZbyaK4qXA/lwKZ60KZrX1fkAraj1pRAPjJStg+0DuFAvzunstHtHbOUTIugu86hntiNnCrlHaejPdVfYaxb51qZWsu3dAO73qq3aM/gdw7Se8qt3o2sYABm8Zy405ZL2ETEec5NMPRsGvW+YpfV+9U66hgKoBg/hvPFxzhSPT1SMnG0K83Kb+YZaG5VxYHZRnJxIGwkAGbygp3cBvNFpRCZ53iUgNtxp4q+qzWgvU7bWVk7mlxnHQfyhYVp7au0iAyAD53VtGwEv+oXA4K9SSG24pAxfaLbVrQA97dFXN1K0Nne21FvVRQpABv0lo66zifZ5THyseTeYLwbYdP2TM8WVBnDkACqvQ7ZpKdg+EgBCUGl3zhQLpJNYAFRmQlZ72GbE1hVi5neNovVEbAAuzwS1A0x7gw6nT/0gmoURKwCZeG82/Qwzv+GziEAyYjwUO4DLELTXmLElUDV+xISXEgGg8jpk0ntB3OGnjgCaTxMDYPQChrx/OO7wFaBAJ+nJxACQmZ66pSmrlG0JoeH3E2LbCE02JPIiZlloXUFOkE5DXvP810TNgEJG2wHCsx6L8G3OwOHEAIj6koakxoS1iQFQ0DXZi5z0VOd7qOsJR79UJwLAT7OaW9KpcjHUAh2csW0vMQZGjiUCQCGjbgTRrggBrM6ZQi628R2GxhYb1i10FwC7mOy3jDFfpRMxA8IEQKCvbfBMAm4GMAKgD4QfiJX9bebwuCP4NQcAwGCpJNru+h2XXMyIa/MVIOaFbUXr+/8tgOoKP2UAhN0im3IATsyHOv0PdRBEN7oZNQeb33KmcH3ZIxGLYKUpoqd3MzjwFyCvt9ATA0B+iCXQiSAzgIAeRROL5pzBv279JAaATDiE/cCVHd6UBBAAwkUAHdXtrdviE7MVrk34VEZ7MEXYwsA9zsXwkbKNLe0D1nfOtuMtEvUK1KYnewRQlAeIoYNIB7MOgs7AGblugrDf6Bf7/RRe1fwHyQ8FfGHScxoAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/pages/demo/star/index.tsx":
/*!***************************************!*\
  !*** ./src/pages/demo/star/index.tsx ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_tsx_taro_type_template_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.tsx?taro&type=template&parse=PAGE& */ "./src/pages/demo/star/index.tsx?taro&type=template&parse=PAGE&");
/* harmony import */ var _index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.tsx?taro&type=script&parse=PAGE& */ "./src/pages/demo/star/index.tsx?taro&type=script&parse=PAGE&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));




/***/ }),

/***/ "./src/pages/demo/star/index.tsx?taro&type=script&parse=PAGE&":
/*!********************************************************************!*\
  !*** ./src/pages/demo/star/index.tsx?taro&type=script&parse=PAGE& ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./index.tsx?taro&type=script&parse=PAGE& */ "./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/pages/demo/star/index.tsx?taro&type=script&parse=PAGE&");
/* harmony import */ var _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_script_parse_PAGE___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/pages/demo/star/index.tsx?taro&type=template&parse=PAGE&":
/*!**********************************************************************!*\
  !*** ./src/pages/demo/star/index.tsx?taro&type=template&parse=PAGE& ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!file-loader?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!../../../../node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!../../../../node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js??ref--6-0!./index.tsx?taro&type=template&parse=PAGE& */ "./node_modules/file-loader/dist/cjs.js?name=[path][name].wxml&context=/Users/jrack/Desktop/future/JUI/src!./node_modules/@tarojs/mini-runner/dist/loaders/miniTemplateLoader.js!./node_modules/@tarojs/mini-runner/dist/loaders/wxTransformerLoader.js?!./src/pages/demo/star/index.tsx?taro&type=template&parse=PAGE&");
/* harmony import */ var _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_PAGE___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _file_loader_name_path_name_wxml_context_Users_jrack_Desktop_future_JUI_src_node_modules_tarojs_mini_runner_dist_loaders_miniTemplateLoader_js_node_modules_tarojs_mini_runner_dist_loaders_wxTransformerLoader_js_ref_6_0_index_tsx_taro_type_template_parse_PAGE___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./src/pages/demo/star/style.scss":
/*!****************************************!*\
  !*** ./src/pages/demo/star/style.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./src/pages/demo/star/index.tsx","runtime","taro","vendors"]]]);