import { ComponentType } from 'react';

interface AddressEntity {
  /** 地区名称 */
  name: string;
  /** 地区code */
  code: string | number;
  /** 是否有下级 */
  hasChildren: boolean;
}

interface AddressPickerProps {
  /** 地址请求接口
   * @param parentCode 地区父级Id  0为最外层
    */
  getAreaApi: (parentCode: number | string) => Promise<Array<AddressEntity>>;

  /** 关闭弹窗
   */
  onClose: () => void;

  /** 是否显示”确定“按钮
   */
  showCommit: boolean;

  /** 改变事件
   * @param namesMap 名称数组
   * @param codesMap code数组
   */
  onChange?: (namesMap: [], codesMap: []) => void;

  /** ”确定“点击事件
   * @param namesMap 名称数组
   * @param codesMap code数组
   */
  onCommit?: (namesMap: [], codesMap: []) => void;

  /** 默认选中 code数组
   */
  defaultValue?: Array<string | number>;

  /** 弹窗标题
   */
  title?: string;
}

/** 图片预览
 *
 * @supported weapp
 * @example
 * ```tsx
 *
 *  state = {
 *    namesMap: [],
 *    codesMap: [],
 *    showAddress: false,
 *  }
 *
 *  onSelectAddress = () => {
 *    this.setState({
 *      showAddress: true,
 *    })
 *  }
 *
 *  onChange = (namesMap, codesMap) => {
 *    this.setState({
 *      namesMap,
 *      codesMap
 *    })
 *  }
 *
 *  getAreaApi = async (code = 0) => {
 *    return mockData.filter(data => data.parent == code);
 *  }
 *
 *  onCommit = (namesMap, codesMap) => {
 *    this.setState({
 *      namesMap,
 *      codesMap
 *    })
 *  }
 *
 *  render() {
 *    const { namesMap, codesMap, showAddress } = this.state;
 *    return (
 *      <View>
 *        <Button type="primary" onClick={this.onSelectAddress}>选择省市县</Button>
 *        <View className="select-name">{namesMap.join(' > ')}</View>
 *        <View className="select-code">{codesMap.join(', ')}</View>
 *        {
 *          showAddress && <AddressPicker
 *            showCommit
 *            onClose={() => {
 *              this.setState({
 *                showAddress: false
 *              })
 *            }}
 *            defaultValue={[1000, 1001]}
 *            onCommit={this.onCommit}
 *            onChange={this.onChange}
 *            getAreaApi={this.getAreaApi} />
 *        }
 *      </View>
 *    )
 *  }
 * ```
 */
declare const AddressPicker: ComponentType<AddressPickerProps>;

export default AddressPicker
