import { ComponentType } from 'react';

/** 图片的尺寸 */
interface sizeType {
  /** 原图 */
  original;
  /** compressed */
  compressed;
}

/** 图片的来源 */
interface sourceType {
  /** 从相册选图 */
  album;
  /** 使用相机 */
  camera;
  /** 使用前置摄像头(仅H5纯浏览器使用) */
  user;
  /** 使用后置摄像头(仅H5纯浏览器) */
  environment;
}

/** 图片的本地临时文件列表 */
interface ImageFile {
  /** 本地临时文件路径 */
  path: string
  /** 本地临时文件大小，单位 B */
  size: number
  /** 文件的 MIME 类型
   * @supported h5
   */
  type?: string
  /** 原始的浏览器 File 对象
   * @supported h5
   */
  originalFileObj?: File
}

interface tempFile {
  /** 图片的本地临时文件路径 */
  url: string
  /** 图片的本地临时文件 */
  file: ImageFile
  /** 文件状态 beforeUpload: 上传之前, uploading: 上传中, error: 上传失败, done: 上传成功  */
  status: 'beforeUpload' | 'uploading' | 'error' | 'done'
}

interface ImagePickerProps {
  /** 最大上传数量
   * @default 8
   */
  maxCount?: number;

  /** 所选的图片的尺寸
   * @default ['original', 'compressed']
   */
  sizeType?: Array<keyof sizeType>;

  /** 选择图片的来源
   * @default ['album', 'camera']
   */
  sourceType?: Array<keyof sourceType>;

  /** 文件列表 */
  files: Array<tempFile>;

  /** 文件列表改变触发事件
   * @param files 新的文件列表
   * @param type 触发改变类型 add:新增，remove:删除
   * @param index 删除时携带删除索引
   */
  onChange: (files: Array<tempFile>, type: 'add' | 'remove', index?: number) => void;

  /** 图片点击事件
   * @param index 图片点击索引
   */
  onImageClick?: (index: number) => void;

}

/** 上传图片
 *
 * @supported weapp
 * @example
 * ```tsx
 *
 *  state = {
 *    files: [],
 *    showImages: false,
 *    showImageIndex: 0,
 *    images: []
 *  }
 *
 *  onChange = files => {
 *    files.forEach((file, index) => {
 *      // 没有上传的 安排上传
 *      if (file.status == 'beforeUpload') {
 *        this.upload(file)
 *      }
 *    })
 *
 *    this.setState({
 *      files
 *    })
 *  }
 *
 *  upload = file => {
 *    file.status = 'uploading';
 *
 *    // TODO 此处调用上传接口，上传成功更改file状态为"done"，上传失败修改为"error"
 *    setTimeout(() => {
 *      file.status = ['done', 'error'][~~(Math.random() * 2)];
 *      this.setState()
 *    }, 1000);
 *  }
 *
 *  onShowImages = (images, showImageIndex) => {
 *    this.setState({
 *      showImageIndex,
 *      showImages: true,
 *      images
 *    })
 *  }
 *
 *  render() {
 *    const { files, showImages, showImageIndex, images } = this.state;
 *    return (
 *      <View className="upload-container">
 *        <ImagePicker maxCount={MAX_UPLOAD} files={files}
 *          onChange={this.onChange}
 *          onImageClick={imageIndex => {
 *            this.onShowImages(files.map(file => file.url), imageIndex)
 *          }}></ImagePicker>
 *        {
 *          showImages && <ImageModal images={images} defaultCurrent={showImageIndex} onClick={() => {
 *            this.setState({
 *              showImages: false
 *            })
 *          }}></ImageModal>
 *        }
 *      </View>
 *    )
 *  }
 * ```
 */
declare const ImagePicker: ComponentType<ImagePickerProps>;

export default ImagePicker
