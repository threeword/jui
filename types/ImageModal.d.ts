import { ComponentType } from 'react';

interface ImageModalProps {
  /** 图片的本地临时文件路径
   */
  images: Array<string>;

  /** 显示图片索引
   * @default 0
   */
  defaultCurrent?: number;

  /** 图片点击事件 */
  onClick?: (imageIndex: number) => void;

}

/** 图片预览
 *
 * @supported weapp
 * @example
 * ```tsx
 *
 *  state = {
 *    files: [],
 *    showImages: false,
 *    showImageIndex: 0,
 *    images: []
 *  }
 *
 *  onChange = files => {
 *    files.forEach((file, index) => {
 *      // 没有上传的 安排上传
 *      if (file.status == 'beforeUpload') {
 *        this.upload(file)
 *      }
 *    })
 *
 *    this.setState({
 *      files
 *    })
 *  }
 *
 *  upload = file => {
 *    file.status = 'uploading';
 *
 *    // TODO 此处调用上传接口，上传成功更改file状态为"done"，上传失败修改为"error"
 *    setTimeout(() => {
 *      file.status = ['done', 'error'][~~(Math.random() * 2)];
 *      this.setState()
 *    }, 1000);
 *  }
 *
 *  onShowImages = (images, showImageIndex) => {
 *    this.setState({
 *      showImageIndex,
 *      showImages: true,
 *      images
 *    })
 *  }
 *
 *  render() {
 *    const { files, showImages, showImageIndex, images } = this.state;
 *    return (
 *      <View className="upload-container">
 *        <ImagePicker maxCount={MAX_UPLOAD} files={files}
 *          onChange={this.onChange}
 *          onImageClick={imageIndex => {
 *            console.log('ImagePicker','你点击了图',index)
 *            this.onShowImages(files.map(file => file.url), imageIndex)
 *          }}></ImagePicker>
 *        {
 *          showImages && <ImageModal images={images} defaultCurrent={showImageIndex} onClick={(index) => {
 *            console.log('ImageModal','你点击了图',index)
 *            this.setState({
 *              showImages: false
 *            })
 *          }}></ImageModal>
 *        }
 *      </View>
 *    )
 *  }
 * ```
 */
declare const ImageModal: ComponentType<ImageModalProps>;

export default ImageModal
