import { ComponentType } from 'react';

interface SwiperDotsProps {
  /** 轮播数量 */
  count: number;

  /** 当前轮播第几页 */
  current: number;

  /** 距离容器下方距离 注意：容器必须使用position
   * @default 20
   */
  bottom?: number;

  /** 选中和非选中 dot 颜色
   * @param color0 未选中颜色
   * @param color1 已选中颜色
   * @default ['#e0e0e0', '#b1b1b1']
   */
  colors?: [color0, color1];

  /** dot 类型 可选 'line' 长方形
   * @default '' 圆形
   */
  type?: '' | 'line'

}

/** 轮播图自定义dot
 *
 * @supported weapp
 * @example
 * ```tsx
 *
 *  state = {
 *    current1: 0,
 *    current2: 0,
 *    data: ['广告1', '广告2', '广告3']
 *  };
 *  
 *
 *  onSwiperChange = index => e => {
 *    this.setState({
 *      [`current${index}`]: e.detail.current
 *    });
 *  };
 *
 *  render() {
 *    const { current1, current2, data } = this.state;
 *    return (
 *      <ScrollView scrollY className="demo-body">
 *        <View>
 *          <View className="demo-title">小圆dot</View>
 *          <View className="demo-content" style="position:relative">
 *            <Swiper
 *              onChange={this.onSwiperChange(1)}
 *              className="swiper-content"
 *              autoplay
 *            >
 *              {data.map(item => (
 *                <SwiperItem>
 *                  <View className="swiper-item">{item}</View>
 *                </SwiperItem>
 *              ))}
 *            </Swiper>
 *            <SwiperDots count={data.length} current={current1}></SwiperDots>
 *          </View>
 *        </View>
 *        <View>
 *          <View className="demo-title">线段dot、自定义颜色</View>
 *          <View className="demo-content" style="position:relative">
 *            <Swiper
 *              onChange={this.onSwiperChange(2)}
 *              className="swiper-content"
 *              autoplay
 *            >
 *              {data.map(item => (
 *                <SwiperItem>
 *                  <View className="swiper-item">{item}</View>
 *                </SwiperItem>
 *              ))}
 *            </Swiper>
 *            <SwiperDots type="line" colors={['#eee', '#000']} count={data.length} current={current2}></SwiperDots>
 *          </View>
 *        </View>
 *      </ScrollView>
 *    );
 *  }
 * ```
 */
declare const SwiperDots: ComponentType<SwiperDotsProps>;

export default SwiperDots
