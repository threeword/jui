import { ComponentType } from 'react';

interface DrawerProps {
  /** 关闭弹窗事件 */
  onClose: () => void;

  /** 抽屉宽度，如果出现方向是'bottom'，就是高度 
   * @default 400
   */
  size?: number;

  /** 抽屉出现方向
   * @default right
   */
  type?: 'left' | 'right' | 'bottom';
}

/** 抽屉
 *
 * @supported weapp
 * @example
 * ```tsx
 *
 *  state = {
 *    showDrawer: false,
 *    type: 'right',
 *  }
 *
 *  render() {
 *    const { showDrawer, type } = this.state;
 *    return (
 *      <View className="demo-drawer-container">
 *        <ALine></ALine>
 *        <ScrollView scrollY className="demo-body">
 *          <View>
 *            <View className="demo-title">右侧弹出抽屉</View>
 *            <View className="demo-content">
 *              <View onClick={() => {
 *                this.setState({
 *                  showDrawer: true,
 *                  type: 'right'
 *                })
 *              }} className="demo-btn">显示Drawer</View>
 *            </View>
 *          </View>
 *          <View>
 *            <View className="demo-title">左侧抽屉弹出</View>
 *            <View className="demo-content">
 *              <View onClick={() => {
 *                this.setState({
 *                  showDrawer: true,
 *                  type: 'left'
 *                })
 *              }} className="demo-btn">显示Drawer</View>
 *            </View>
 *          </View>
 *          <View>
 *            <View className="demo-title">底部抽屉弹出</View>
 *            <View className="demo-content">
 *              <View onClick={() => {
 *                this.setState({
 *                  showDrawer: true,
 *                  type: 'bottom'
 *                })
 *              }} className="demo-btn">显示Drawer</View>
 *            </View>
 *          </View>
 *          <View>
 *            <View className="demo-title">测试出现滚动，弹窗不会出现滚动</View>
 *            <View className="demo-content">
 *              <View onClick={() => {
 *                this.setState({
 *                  showDrawer: true
 *                })
 *              }} className="demo-btn">显示Drawer</View>
 *            </View>
 *          </View>
 *        </ScrollView>
 *        {
 *          showDrawer
 *          && <View className="demo-drawer-modal-container">
 *            <Drawer type={type} onClose={() => { this.setState({ showDrawer: false }) }}>
 *              <View className="diy-content">
 *                <View className="title">标题</View>
 *                <View className="content">内容</View>
 *                <View className="demo-btn">按钮</View>
 *              </View>
 *            </Drawer>
 *          </View>
 *        }
 *      </View>
 *    )
 *  }
 * ```
 */
declare const Drawer: ComponentType<DrawerProps>;

export default Drawer
