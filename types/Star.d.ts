import { ComponentType } from 'react';

interface StarProps {
  /** 当前评分 */
  count: number;

  /** 最大评分
   * @default 5
   */
  maxStar?: number;

  /** 是否大图标
   * @default false
   */
  large?: boolean;

  /** 是否只读
   * @default false
   */
  readonly?: boolean;

  /** 自定义图标
   * @default 星星
   * starImg0: 未选中图标
   * starImg1: 已选中图标
   */
  starImage?: [starImg0: string, starImg1: string];

  /** 点击值改变事件
   * @param count 当前评分
   */
  onChange?: (count: number) => void;

}

/** 评分
 *
 * @supported weapp
 * @example
 * ```tsx
 *  import starImg_1 from './images/star1.png';
 *  import starImg_0 from './images/star0.png';
 * 
 *  ......
 * 
 *  state = {
 *    starCount1: 2,
 *    starCount2: 0,
 *    starCount3: 0,
 *  }
 * 
 *  render() {
 *    const { starCount1, starCount2, starCount3 } = this.state;
 *    return (
 *      <ScrollView scrollY className="demo-body">
 *        <View>
 *          <View className="demo-title">小星星、只读</View>
 *          <View className="demo-content">
 *            <Star count={starCount1} onChange={count => {
 *              this.setState({ starCount1: count })
 *            }}></Star>
 *          </View>
 *        </View>
 *        <View>
 *          <View className="demo-title">大星星、可编辑</View>
 *          <View className="demo-content">
 *            <Star large count={starCount2} onChange={count => {
 *              this.setState({ starCount2: count })
 *            }}></Star>
 *          </View>
 *        </View>
 *        <View>
 *          <View className="demo-title">自定义图标、大星星、可编辑、八颗星</View>
 *          <View className="demo-content">
 *            <Star starImage={[starImg0, starImg1]} maxStar={8} large count={starCount3} onChange={count => {
 *              this.setState({ starCount3: count })
 *            }}></Star>
 *          </View>
 *        </View>
 *      </ScrollView>
 *    )
 *  }
 * ```
 */
declare const Star: ComponentType<StarProps>;

export default Star
