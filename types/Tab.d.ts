import { ComponentType } from 'react';

interface TabProps {
  /** tab选项
   * @example ['菜单1','菜单二']
   */
  options: Array<string>;

  /** 默认选中项 */
  defaultSelect: string;

  /** 选项改变事件 */
  onChange: (name: string) => Promise<void>;

  /** 是否显示分隔线
   * @default false
   */
  noSplit?: boolean;

  /** tab项自定义样式 */
  style?: object;

  /** 选中时颜色 支持rgb、rgba、#000 等 */
  activeColor?: string;

  /** 选项宽度 默认：自适应宽度 支持：px、em、%、rpx、vw 等 */
  width?: string;

  /** 对应tab项 显示dot  */
  dots?: array;
}

/** 标签页
 *
 * @supported weapp
 * @example
 * ```tsx
 *
 *  state = {
 *    tabName1: '菜单1';
 *    tabName2: '菜单1';
 *    tabName3: '菜单1';
 *  } 
 *
 *  render() {
 *    const { tabName1, tabName2, tabName3 } = this.state;
 *    return (
 *      <View>
 *        <View>
 *          <View className="demo-title">宽度50%，无分隔线</View>
 *          <Tab
 *            noSplit
 *            width="50%"
 *            defaultSelect={'菜单1'} options={['菜单1', '菜单2']} onChange={tabName => {
 *              this.setState({
 *                tabName1: tabName
 *              })
 *            }}></Tab>
 *          <View className="demo-content">{tabName1}</View>
 *        </View>
 *
 *        <View>
 *          <View className="demo-title">宽度自适，有分隔线，有dot</View>
 *          <Tab
 *            dots={[11, 0, 10]}
 *            defaultSelect={'菜单1'} options={['菜单1', '菜单菜单2', '菜单菜3', '菜单菜单菜单4', '菜单5', '菜单6', '菜单7', '菜单8', '菜单9']} onChange={tabName => {
 *              this.setState({
 *                tabName2: tabName
 *              })
 *            }}></Tab>
 *          <View className="demo-content">{tabName2}</View>
 *        </View>
 *
 *        <View>
 *          <View className="demo-title">自定义 默认字体样式 和 选中字体样式</View>
 *          <Tab
 *            noSplit
 *            dots={[11, 0, 10]}
 *            width="33%"
 *            activeColor="#688aef"
 *            style={{ fontWeight: 400, color: "#999" }}
 *            defaultSelect={'菜单1'} options={['菜单1', '菜单菜单2', '菜单3']} onChange={tabName => {
 *              this.setState({
 *                tabName3: tabName
 *              })
 *            }}></Tab>
 *          <View className="demo-content">{tabName3}</View>
 *        </View>
 *      </View>
 *    )
 *  }
 * ```
 */
declare const Tab: ComponentType<TabProps>;

export default Tab
