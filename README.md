## @threeword/jui

是一款基于 Taro 框架开发的 UI 组件库 (持续更新)

## 特色

- typescript
- 需要定制的留言 安排 :)

## 安装（Install）

```bash
# npm
npm i @threeword/jui -S

# yarn
yarn add @threeword/jui
```

## 小程序演示

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/gh_7c5d4cf235cb_430.jpg)

## 组件展示

> 评分

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/star.png)

> 标签页

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/tab.png)

> 轮播图自定义 dot

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/swiperDots.png)

> 抽屉

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/drawer.png)

> 图片上传

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/imagePicker.png)

> 图片预览

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/imageModal.png)

> 地址选择器

![JUI](http://weapp-54548.oss-cn-hangzhou.aliyuncs.com/address.png)

## 引入组件（具体参考 .d 文件)

```tsx
import { ImageModal } from '@threeword/jui';

<ImageModal images={['图片地址1', '图片地址2']} />;
```

## 参与贡献

- Fork 本仓库
- 新建 Feature_xxx 分支
- 提交代码
- 新建 Pull Request
