import { ComponentClass } from "react";
import Taro, { Component, Config } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import classNames from "classnames";
import "./style.scss";

type PageOwnProps = {
  count: Number;
  current: Number;
  bottom?: Number;
  colors?: Array;
  type?: String | 'line';
};

type PageState = {};

type IProps = PageOwnProps;

interface Page {
  props: IProps;
}

class Page extends Component {
  state = {};

  render() {
    const { type = "", count, current, bottom = 20, colors = ['#e0e0e0', '#b1b1b1'] } = this.props;
    const arr = new Array(~~count).fill(0);
    const [color0, color1] = colors;

    const convertBottom = Taro.pxTransform(bottom)

    return (
      <View className="dots-c" style={{ bottom: convertBottom }}>
        {arr.map((e, i) => (
          <View
            className={classNames("dot", type, {
              active: i == current,
            })}
            style={{ background: i == current ? color1 : color0 }}
          ></View>
        ))}
      </View>
    );
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>;
