import { ComponentClass, ChangeEvent } from "react";
import Taro, { Component, Config } from "@tarojs/taro";
import {
  View,
  Text,
  Image,
  PickerView,
  PickerViewColumn
} from "@tarojs/components";
import "./style.scss";

type PageOwnProps = {
  getAreaApi: Function;
  onClose: Function;

  showCommit: Boolean;
  onChange?: Function;
  onCommit?: Function;
  defaultValue?: Array;
  title?: String;
};

type PageState = {};

type IProps = PageOwnProps;

const SELECT_TIP = ["选择省", "选择市", "选择区县", "选择街道"];
interface Page {
  props: IProps;
}

class Page extends Component {
  state = {
    showSelect: false
  };

  constructor(props) {
    super(props);

    this.options = [];
    this.nextLoading = false;

    this.state = {
      value: [],
      selectObj: [],
      selectIndex: -1,
      showNext: false
    };
  }

  componentDidMount() {
    this.selectOption();
  }

  selectOption = async () => {
    const { defaultValue = [] } = this.props;
    if (defaultValue.length > 0 && this.state.selectObj.length == 0) {
      const _initValue = await this.initData(defaultValue, [], []);
      const lastSelectOption = _initValue.result[_initValue.result.length - 1];
      const lastSelectIndex = _initValue.options.findIndex(
        option => option.code == lastSelectOption.code
      );
      this.options = _initValue.options;
      this.setState({
        selectObj: _initValue.result,
        value: [lastSelectIndex],
        selectIndex: _initValue.result.length - 1,
        showNext: !!lastSelectOption.hasChildren
      });
    } else {
      this.selectNext();
    }
  }

  initData = async (value, result, options) => {
    if (value.length == result.length) {
      return { result, options };
    } else {
      const _options = await this.props.getAreaApi(result.length == 0 ? 0 : result[result.length - 1].code);
      result[result.length] = _options.find(
        o => o.code == value[result.length]
      );
      return await this.initData(value, result, _options);
    }
  }

  async componentWillReceiveProps(nextProps) {
    const { value = [] } = nextProps;
    if (value.length > 0 && this.state.selectObj.length == 0) {

      const _initValue = this.initData(value, [], []);
      console.log(_initValue);
      const lastSelectOption = _initValue.result[_initValue.result.length - 1];
      const lastSelectIndex = _initValue.options.findIndex(
        option => option.code == lastSelectOption.code
      );
      this.options = _initValue.options;
      this.setState({
        selectObj: _initValue.result,
        value: [lastSelectIndex],
        selectIndex: _initValue.result.length - 1,
        showNext: lastSelectOption.hasChildren
      });
    }
  }

  async getArea(parentId) {
    this.options = await this.props.getAreaApi(parentId);
    this.setState();
  }

  onChange = e => {
    if (this.nextLoading) return;
    const { selectObj, selectIndex } = this.state;
    const value = e.detail.value;
    /* 改变时赋值，并判断是否有下一级 */
    selectObj[selectIndex] = { ...this.options[value[0]] };
    this.setState({
      selectObj,
      value,
      showNext: selectObj[selectIndex].hasChildren
    });
    this.props.onChange(selectObj.map(o => o.name), selectObj.map(o => o.code));
  };

  /* 选择下一级 */
  selectNext = async () => {
    if (this.nextLoading) return;
    this.nextLoading = true;
    const { selectObj, selectIndex } = this.state;
    const nowSelectObj = selectObj[selectIndex];
    this.options = [];
    /* 当前下一级 */
    this.setState({
      selectIndex: selectIndex + 1
    });
    /* 请求数据 */
    await this.getArea(nowSelectObj && nowSelectObj.code);
    /* 默认选中下一级第一个 */
    if (this.options.length > 0) {
      selectObj[selectIndex + 1] = { ...this.options[0] };
      this.setState({
        value: [0],
        selectObj,
        showNext: !!selectObj[selectIndex + 1].hasChildren
      });
      this.props.onChange(selectObj.map(o => o.name), selectObj.map(o => o.code));
    } else {
      this.setState({ value: [] });
      this.props.onChange([], []);
    }
    this.nextLoading = false;
  };

  /* 重新选择 */
  reSelect = selectIndex => async () => {
    const { selectObj } = this.state;
    const reSelectObj = selectObj[selectIndex];
    /* 判断是否是第一集 */
    if (selectIndex <= 0) {
      await this.getArea();
    } else {
      await this.getArea(selectObj[selectIndex - 1].code);
    }
    /* 选中值 */
    const value = this.options.findIndex(o => o.code == reSelectObj.code);
    const newSelectObj = [...selectObj].slice(0, selectIndex + 1);
    this.setState({
      value: [value],
      selectIndex,
      selectObj: newSelectObj,
      showNext: reSelectObj.hasChildren
    });
    this.props.onChange(newSelectObj.map(o => o.name), newSelectObj.map(o => o.code));
  };

  onCommit = () => {
    const { selectObj } = this.state;
    this.props.onCommit(selectObj.map(o => o.name), selectObj.map(o => o.code));
    this.props.onClose();
  }

  render() {
    const {
      value,
      selectIndex,
      selectObj,
      showNext,
      showSelect
    } = this.state;

    const { showCommit, onClose, title = "所在地区" } = this.props;

    return (
      <View className="address-c">
        <View className="address-select-c">
          {/* 黑色背景 */}
          <View
            className="modal-shadow"
            onClick={onClose}
          ></View>
          <View className="address-select-box">
            {/* 标题 */}
            <View className="address-select-title">
              {title}
              {showCommit && <View className="address-select-btn" onClick={this.onCommit}>确定</View>}
            </View>
            {/* 所选列表 */}
            <View className="address-select-text">
              {selectObj.map((obj, i) => (
                <View
                  className="address-select-item-c"
                  onClick={this.reSelect(i)}
                >
                  <View className="text">{obj.name}</View>
                  {i == selectIndex && <View className="item-line"></View>}
                </View>
              ))}
              {/* 下一级 */}
              {showNext && (
                <View
                  onClick={this.selectNext}
                  className="address-select-item-c"
                >
                  <View className="text select-btn">
                    {SELECT_TIP[selectIndex + 1]}
                  </View>
                  {selectObj.length == selectIndex && (
                    <View className="item-line"></View>
                  )}
                </View>
              )}
            </View>
            {/* 选择组件 */}
            <PickerView
              indicatorStyle="height: 50px;line-height:50px;"
              style="width: 100%;text-align:center;height:422px;"
              value={value}
              onChange={this.onChange}
            >
              <PickerViewColumn>
                {this.options.map(option => (
                  <View className="select-option">{option.name}</View>
                ))}
              </PickerViewColumn>
            </PickerView>
          </View>
        </View>
      </View>
    );
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>;
