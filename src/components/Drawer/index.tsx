import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import classNames from 'classnames'
import './style.scss'

import starImg_1 from './images/star1.png';
import starImg_0 from './images/star0.png';

type PageOwnProps = {
  onClose: Function;
  type: 'left' | 'right' | 'bottom';
  size: number;
}

type PageState = {}

type IProps = PageOwnProps

interface Page {
  props: IProps;
}

class Page extends Component {
  state = {

  }

  render() {
    const { children, onClose, type = "right", size = 400 } = this.props;

    const length = Taro.pxTransform(size)
    let style = { width: length };
    if (type == 'bottom') {
      style = { height: length }
    }

    return (
      <View className="drawer-container">
        <View onClick={onClose} className="drawer-shadow"></View>
        <View
          className={classNames('drawer-content', type)}
          style={style}>{children}</View>
      </View>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>