import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import classNames from 'classnames'
import './style.scss'

import starImg_1 from './images/star1.png';
import starImg_0 from './images/star0.png';

type PageOwnProps = {
  maxStar: Number;
  count: Number;
  large: Boolean;
  readonly: Boolean;
  onChange: Function;
  starImage: Array;
}

type PageState = {}

type IProps = PageOwnProps

interface Page {
  props: IProps;
}

class Page extends Component {
  state = {

  }

  calcCount = (maxStar, count) => {
    return new Array(maxStar).fill(0).map((v, i) => ~~(i < count))
  }

  onCountClick = count => {
    if (this.props.readonly) return;
    this.props.onChange(count + 1);
  }

  render() {
    const { maxStar = 5, count = 0, large = false, starImage = [] } = this.props;
    const arr = this.calcCount(maxStar, count);
    let [starImg0, starImg1] = [starImg_0, starImg_1]

    /* 替换为传入图片 */
    if (starImage.length > 1) {
      starImg0 = starImage[0];
      starImg1 = starImage[1];
    }

    return (
      <View className={classNames('start-c', {
        large
      })}>
        {arr.map((v, index) => v
          ? <Image mode="aspectFit" onClick={() => {
            this.onCountClick(index)
          }} className="start-i" src={starImg1}></Image>
          : <Image mode="aspectFit" onClick={() => {
            this.onCountClick(index)
          }} className="start-i" src={starImg0}></Image>
        )}
      </View>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>