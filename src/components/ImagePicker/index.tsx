import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './style.scss'

import mediaImg from './images/media.png'
import loadingImg from './images/loading.png'
import closeImg from './images/close.png'
import errorImg from './images/error.png'

type PageOwnProps = {
  maxCount?: Number;
  sizeType?: Array;
  sourceType?: Array;
  files: Array;
  onChange: Function;
  onImageClick?: Function;
}

type PageState = {}

type IProps = PageOwnProps

interface Page {
  props: IProps;
}

class Page extends Component {

  chooseFile = async () => {
    try {
      const { files = [], maxCount = 8, sizeType = ['original', 'compressed'], sourceType = ['album', 'camera'] } = this.props
      const selectCount = maxCount - files.length;
      if (selectCount < 1) return;
      const res = await Taro.chooseImage({
        files,
        count: selectCount,
        sizeType,
        sourceType
      });
      const targetFiles = res.tempFilePaths.map((path, i) => ({
        url: path,
        file: res['tempFiles'][i],
        status: 'beforeUpload',
      }));
      const newFiles = files.concat(targetFiles);
      this.props.onChange(newFiles, 'add');
    } catch (e) {
      console.error(e);
    }
  }

  removeFile = index => () => {
    const { files = [] } = this.props
    const newFiles = files.filter((_, i) => i !== index)
    this.props.onChange(newFiles, 'remove', index)
  }

  render() {
    const { files = [], maxCount = 8, onImageClick = () => { } } = this.props;
    return (
      <View className="image-picker-c">
        {files.map((file, index) =>
          <View taroKey={file.url} key={file.url} className="image-picker-upload-item">
            <View className="image-picker-remove" onClick={this.removeFile(index)}>
              <Image className="img" src={closeImg}></Image>
            </View>
            <Image mode="aspectFit" onClick={() => onImageClick(index)} className="image-picker-img" src={file.url}></Image>
            {
              ['beforeUpload', 'uploading'].includes(file.status)
              && <View className="image-picker-loading">
                <Image className="img loading" src={loadingImg}></Image>
              </View>
            }
            {
              ['error'].includes(file.status)
              && <View className="image-picker-loading">
                <Image className="img" src={errorImg}></Image>
              </View>
            }
          </View>)}
        {
          files.length < maxCount && <View className="image-picker-upload-item" onClick={this.chooseFile}>
            <Image className="image-picker-upload-img" src={mediaImg}></Image>
          </View>
        }
      </View>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>