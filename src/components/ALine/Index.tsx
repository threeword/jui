import { ComponentClass } from "react";
import Taro, { Component, Config } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import "./style.scss";

type PageOwnProps = {};

type PageState = {};

type IProps = PageOwnProps;

interface Page {
  props: IProps;
}

class Page extends Component {
  state = {};

  render() {
    return <View className="line"></View>;
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>;
