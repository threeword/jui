import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image, Swiper, SwiperItem } from '@tarojs/components'
import './style.scss'

type PageOwnProps = {
  defaultCurrent?: Number;
  images: Array;
  onClick?: Function;
}

type PageState = {}

type IProps = PageOwnProps

interface Page {
  props: IProps;
}

class Page extends Component {
  state = {
    current: this.props.defaultCurrent || 0,
  };

  onSwiperChange = e => {
    this.setState({
      current: e.detail.current
    });
  };


  render() {
    const { onClick = () => { }, images = [] } = this.props;
    const { current } = this.state;
    return (
      <View className="image-modal-c">
        <View className="image-title">{`${current + 1}/${images.length}`}</View>
        <Swiper
          onChange={this.onSwiperChange}
          current={current}
          className="swiper-content"
        >
          {
            images.map((imageUrl, index) => <SwiperItem taroKey={imageUrl} key={imageUrl}>
              <Image
                onClick={() => onClick(index)}
                mode="aspectFit"
                className="image"
                src={imageUrl}
              ></Image>
            </SwiperItem>)
          }
        </Swiper>
      </View>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>