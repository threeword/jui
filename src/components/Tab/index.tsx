import { ComponentClass } from "react";
import Taro, { Component, Config } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import classNames from "classnames";

import "./index.scss";

type PageOwnProps = {
  options: Array;
  onChange: Function;
  defaultSelect: String;
  noSplit: Boolean;
  style: Object;
  activeColor: String;
  width: String;
  dots: Array;
};

type PageState = {};

type IProps = PageOwnProps;

interface AComponent {
  props: IProps;
}

class AComponent extends Component {
  state = {
    select: null
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.select) return null;
    return {
      select: nextProps.defaultSelect
    };
  }

  render() {
    const { select } = this.state;
    const {
      style,
      activeColor = false,
      width,
      dots,
      noSplit,
      options = ["菜单一", "菜单二"],
      onChange = () => { }
    } = this.props;
    return (
      <ScrollView className="seckilling-content" scrollX>
        <View className="tab-c">
          {options.map((name, i) => {
            const active = name == select;
            let newStyle = { ...style };
            const tabActiveStyle = {}

            if (active && activeColor) {
              newStyle['color'] = `${activeColor} !important`;
              tabActiveStyle['background'] = `${activeColor} !important`
            }


            return <View
              onClick={async () => {
                this.setState({
                  select: name
                });
                await onChange(name);
              }}
              className={classNames("tab-i", {
                active,
                "no-split": noSplit
              })}
              style={{ width }}
              key={name}
            >
              <View className="tab-t" style={newStyle}>
                {name}
                {dots[i] && <View className="dot">{dots[i]}</View>}
              </View>
              <View className="tab-active" style={{ ...tabActiveStyle }}></View>
            </View>
          })}
        </View>
      </ScrollView >
    );
  }
}

/** 视图容器
 * @example
 * ```tsx
 * <Tab
 *  defaultSelect="猜你喜欢"
 *  options={[
 *    "猜你喜欢",
 *    "居家生活",
 *  ]}
 *  onChange={name => console.log(name)}
 *></Tab>
 * ```
 */
export default AComponent as ComponentClass<PageOwnProps, PageState>;
