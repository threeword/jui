/** 页面提示
 *  @param title: 提示内容
 *  @param cb: 结束时回调
 */
export declare function showToast(title, cb): void;

/** 小程序自动更新
 */
export declare function updateSelf(): any;

/** 万无一失
 * @param func 执行函数
 * @param defaults 默认值
 */
export declare function ensure(func, defaults): any;

/** uuid
 * @param len 长度 默认8
 * @param radix 随机范围 默认16
 */
export declare function uuid(len, radix): any;

/** 延时函数
 * @param delayTime 延时时间 默认 0.5秒
 */
export declare function sleep(delayTime): any;

