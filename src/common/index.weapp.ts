import Taro from "@tarojs/taro";

export function showToast(title, cb = () => { }) {
  const duration = 3000;

  Taro.showToast({
    title,
    icon: "none",
    mask: true,
    duration
  });
  
  setTimeout(cb, duration);
}

export function updateSelf() {
  const updateManager = Taro.getUpdateManager();
  updateManager.onCheckForUpdate(function (res) {
    // 请求完新版本信息的回调
    if (res.hasUpdate) {
      showToast("新版本已上线，即将更新");
    }
  });
  updateManager.onUpdateReady(function () {
    updateManager.applyUpdate();
  });
  updateManager.onUpdateFailed(function () {
    // 新的版本下载失败
    console.error(res.hasUpdate);
  });
}

export function ensure(func, defaults) {
  try {
    return func();
  } catch (e) {
    return defaults;
  }
}

export function uuid(len = 8, radix = 16) {
  const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split(
    ""
  );
  const value: string[] = [];
  let i = 0;
  radix = radix || chars.length;

  if (len) {
    // Compact form
    for (i = 0; i < len; i++) value[i] = chars[0 | (Math.random() * radix)];
  } else {
    // rfc4122, version 4 form
    let r;

    // rfc4122 requires these characters
    /* eslint-disable-next-line */
    value[8] = value[13] = value[18] = value[23] = "-";
    value[14] = "4";

    // Fill in random data.  At i==19 set the high bits of clock sequence as
    // per rfc4122, sec. 4.1.5
    for (i = 0; i < 36; i++) {
      if (!value[i]) {
        r = 0 | (Math.random() * 16);
        value[i] = chars[i === 19 ? (r & 0x3) | 0x8 : r];
      }
    }
  }

  return value.join("");
}

export function sleep(delayTime = 500) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, delayTime);
  });
}
