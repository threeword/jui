import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, ScrollView } from '@tarojs/components'
import './index.scss'

import uploadIcon from 'src/assets/images/upload.png'
import rightIcon from 'src/assets/images/right.png'
import addressIcon from 'src/assets/images/address.png'
import tabIcon from 'src/assets/images/tab.png'
import starIcon from 'src/assets/images/star.png'
import swiperIcon from 'src/assets/images/swiper.png'
import drawerIcon from 'src/assets/images/drawer.png'

export default class Index extends Component {

  config: Config = {
    navigationBarTitleText: 'JUI'
  }

  componentData = [{
    title: '上传和预览',
    remark: '上传和预览',
    icon: uploadIcon,
    path: '/pages/demo/upload/index'
  }, {
    title: '省市县选择',
    remark: '省市县',
    icon: addressIcon,
    path: '/pages/demo/address/index'
  }, {
    title: '标签栏',
    remark: '标签栏',
    icon: tabIcon,
    path: '/pages/demo/tab/index'
  }, {
    title: '评分',
    remark: '评分',
    icon: starIcon,
    path: '/pages/demo/star/index'
  }, {
    title: 'Swiper自定义dot',
    remark: '轮播图自定义dot',
    icon: swiperIcon,
    path: '/pages/demo/swiper/index'
  }, {
    title: '抽屉',
    remark: '抽屉',
    icon: drawerIcon,
    path: '/pages/demo/drawer/index'
  }]

  render() {
    return (
      <ScrollView scrollY className='index'>
        <View className="logo-c">
          <View className="logo">
            JUI
          </View>
          <View className="version">当前版本：0.0.7</View>
        </View>
        <View className="component-c">
          {
            this.componentData.map(data => <View onClick={() => {
              Taro.navigateTo({
                url: data.path
              })
            }} key={data.title} className="component-box">
              <View className="box-icon">
                <Image className="img" src={data.icon}></Image>
              </View>
              <View className="box-content">
                <View className="title">{data.title}</View>
                <View className="remark">{data.remark}</View>
              </View>
              <View className="box-right">
                <Image className="img" src={rightIcon}></Image>
              </View>
            </View>)
          }
        </View>
      </ScrollView>
    )
  }
}
