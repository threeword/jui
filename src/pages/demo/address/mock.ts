export const mockData = [{
  code: 1000,
  name: '江苏省',
  parent: 0,
  hasChildren:true
}, {
  code: 1001,
  name: '南京市',
  parent: 1000,
  hasChildren:true
}, {
  code: 1002,
  name: '雨花台区',
  parent: 1001,
  hasChildren:true
}, {
  code: 1003,
  name: '铁心桥街道',
  parent: 1002,
}, {
  code: 1004,
  name: '雨花街道',
  parent: 1002,
}, {
  code: 1005,
  name: '板桥街道',
  parent: 1002,
}, {
  code: 1006,
  name: '玄武区',
  parent: 1001,
  hasChildren:true
}, {
  code: 1007,
  name: '新街口街道',
  parent: 1006,
}, {
  code: 1008,
  name: '双塘街道',
  parent: 1006,
}, {
  code: 1009,
  name: '锁金村街道',
  parent: 1006,
}, {
  code: 10010,
  name: '秦淮区',
  parent: 1001,
  hasChildren:true
}, {
  name: '夫子庙街道',
  code: 1011,
  parent: 10010
}, {
  name: '双塘街道',
  code: 1012,
  parent: 10010
}, {
  name: '中华门街道',
  code: 1013,
  parent: 10010
}]