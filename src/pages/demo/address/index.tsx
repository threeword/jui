import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'
import './style.scss'

import { AddressPicker } from 'src/components';

import { mockData } from './mock';

type PageOwnProps = {}

type PageState = {}

type IProps = PageOwnProps

interface Page {
  props: IProps;
}

class Page extends Component {
  config: Config = {
    navigationBarTitleText: '省市县选择器'
  };

  state = {
    namesMap: [],
    codesMap: [],
    showAddress: false,
  }

  onSelectAddress = () => {
    this.setState({
      showAddress: true,
    })
  }

  onChange = (namesMap, codesMap) => {
    this.setState({
      namesMap,
      codesMap
    })
  }

  getAreaApi = async (code = 0) => {
    return mockData.filter(data => data.parent == code);
  }

  onCommit = (namesMap, codesMap) => {
    this.setState({
      namesMap,
      codesMap
    })
  }

  render() {
    const { namesMap, codesMap, showAddress } = this.state;
    return (
      <View>
        <Button type="primary" onClick={this.onSelectAddress}>选择省市县</Button>
        <View className="select-name">{namesMap.join(' > ')}</View>
        <View className="select-code">{codesMap.join(', ')}</View>
        {
          showAddress && <AddressPicker
            showCommit
            onClose={() => {
              this.setState({
                showAddress: false
              })
            }}
            defaultValue={[1000, 1001]}
            onCommit={this.onCommit}
            onChange={this.onChange}
            getAreaApi={this.getAreaApi} />
        }
      </View>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>