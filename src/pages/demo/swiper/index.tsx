import { ComponentClass } from "react";
import Taro, { Component, Config } from "@tarojs/taro";
import { View, Text, Swiper, SwiperItem } from "@tarojs/components";
import "./style.scss";

import SwiperDots from "src/components/SwiperDots";

type PageOwnProps = {
  data: Array;
};

type PageState = {};

type IProps = PageOwnProps;

interface Page {
  props: IProps;
}

class Page extends Component {
  config: Config = {
    navigationBarTitleText: 'Swiper自定义dot'
  };

  state = {
    current1: 0,
    current2: 0,
    data: ['广告1', '广告2', '广告3']
  };
  

  onSwiperChange = index => e => {
    this.setState({
      [`current${index}`]: e.detail.current
    });
  };

  render() {
    const { current1, current2, data } = this.state;
    return (
      <ScrollView scrollY className="demo-body">
        <View>
          <View className="demo-title">小圆dot</View>
          <View className="demo-content" style="position:relative">
            <Swiper
              onChange={this.onSwiperChange(1)}
              className="swiper-content"
              autoplay
            >
              {data.map(item => (
                <SwiperItem>
                  <View className="swiper-item">{item}</View>
                </SwiperItem>
              ))}
            </Swiper>
            <SwiperDots count={data.length} current={current1}></SwiperDots>
          </View>
        </View>
        <View>
          <View className="demo-title">线段dot、自定义颜色</View>
          <View className="demo-content" style="position:relative">
            <Swiper
              onChange={this.onSwiperChange(2)}
              className="swiper-content"
              autoplay
            >
              {data.map(item => (
                <SwiperItem>
                  <View className="swiper-item">{item}</View>
                </SwiperItem>
              ))}
            </Swiper>
            <SwiperDots type="line" colors={['#eee', '#000']} count={data.length} current={current2}></SwiperDots>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>;
