import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image, Button, ScrollView } from '@tarojs/components'
import './style.scss'

import { Tab } from 'src/components';

type PageOwnProps = {}

type PageState = {}

type IProps = PageOwnProps

interface Page {
  props: IProps;
}

class Page extends Component {
  config: Config = {
    navigationBarTitleText: '标签页'
  };

  state = {
    tabName1: '菜单1';
    tabName2: '菜单1';
    tabName3: '菜单1';
  }

  render() {
    const { tabName1, tabName2, tabName3 } = this.state;
    return (
      <ScrollView scrollY className="demo-body">
        <View>
          <View className="demo-title">宽度50%，无分隔线</View>
          <Tab
            noSplit
            width="50%"
            defaultSelect={'菜单1'} options={['菜单1', '菜单2']} onChange={tabName => {
              this.setState({
                tabName1: tabName
              })
            }}></Tab>
          <View className="demo-content">{tabName1}</View>
        </View>

        <View>
          <View className="demo-title">宽度自适，有分隔线，有dot</View>
          <Tab
            dots={[11, 0, 10]}
            defaultSelect={'菜单1'} options={['菜单1', '菜单菜单2', '菜单菜3', '菜单菜单菜单4', '菜单5', '菜单6', '菜单7', '菜单8', '菜单9']} onChange={tabName => {
              this.setState({
                tabName2: tabName
              })
            }}></Tab>
          <View className="demo-content">{tabName2}</View>
        </View>

        <View>
          <View className="demo-title">自定义 默认字体样式 和 选中字体样式</View>
          <Tab
            noSplit
            dots={[11, 0, 10]}
            width="33%"
            activeColor="#688aef"
            style={{ fontWeight: 400, color: "#999" }}
            defaultSelect={'菜单1'} options={['菜单1', '菜单菜单2', '菜单3']} onChange={tabName => {
              this.setState({
                tabName3: tabName
              })
            }}></Tab>
          <View className="demo-content">{tabName3}</View>
        </View>
      </ScrollView>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>