import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image, Button, ScrollView } from '@tarojs/components'
import './style.scss'

import starImg0 from './images/star0.png'
import starImg1 from './images/star1.png'

import { Star } from 'src/components';

type PageOwnProps = {}

type PageState = {}

type IProps = PageOwnProps

interface Page {
  props: IProps;
}

class Page extends Component {
  config: Config = {
    navigationBarTitleText: '评分'
  };

  state = {
    starCount1: 2,
    starCount2: 0,
    starCount3: 0,
  }

  render() {
    const { starCount1, starCount2, starCount3 } = this.state;
    return (
      <ScrollView scrollY className="demo-body">
        <View>
          <View className="demo-title">小星星、只读</View>
          <View className="demo-content">
            <Star count={starCount1} onChange={count => {
              this.setState({ starCount1: count })
            }}></Star>
          </View>
        </View>
        <View>
          <View className="demo-title">大星星、可编辑</View>
          <View className="demo-content">
            <Star large count={starCount2} onChange={count => {
              this.setState({ starCount2: count })
            }}></Star>
          </View>
        </View>
        <View>
          <View className="demo-title">自定义图标、大星星、可编辑、八颗星</View>
          <View className="demo-content">
            <Star starImage={[starImg0, starImg1]} maxStar={8} large count={starCount3} onChange={count => {
              this.setState({ starCount3: count })
            }}></Star>
          </View>
        </View>
      </ScrollView>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>