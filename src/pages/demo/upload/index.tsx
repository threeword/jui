import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './style.scss'

import { ImageModal, ImagePicker } from 'src/components'

type PageOwnProps = {}

type PageState = {}

type IProps = PageOwnProps

/** 最大上传数量 */
const MAX_UPLOAD = 8;

interface Page {
  props: IProps;
}

class Page extends Component {

  config: Config = {
    navigationBarTitleText: '上传和预览'
  };

  state = {
    files: [],
    showImages: false,
    showImageIndex: 0,
    images: []
  }

  onChange = files => {
    files.forEach((file, index) => {
      /* 没有上传的 安排上传 */
      if (file.status == 'beforeUpload') {
        this.upload(file)
      }
    })

    this.setState({
      files
    })
  }

  upload = file => {
    file.status = 'uploading';

    /* TODO 此处调用上传接口，上传成功更改file状态为"done"，上传失败修改为"error" */
    setTimeout(() => {
      file.status = ['done', 'error'][~~(Math.random() * 2)];
      this.setState()
    }, 1000);
  }

  onShowImages = (images, showImageIndex) => {
    this.setState({
      showImageIndex,
      showImages: true,
      images
    })
  }

  render() {
    const { files, showImages, showImageIndex, images } = this.state;
    return (
      <View className="upload-container">
        <ImagePicker maxCount={MAX_UPLOAD} files={files}
          onChange={this.onChange}
          onImageClick={imageIndex => {
            this.onShowImages(files.map(file => file.url), imageIndex)
          }}></ImagePicker>
        {
          showImages && <ImageModal images={images} defaultCurrent={showImageIndex} onClick={() => {
            this.setState({
              showImages: false
            })
          }}></ImageModal>
        }
      </View>
    )
  }
}

export default Page as ComponentClass<PageOwnProps, PageState>